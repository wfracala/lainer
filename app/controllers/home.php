<?php 
namespace App\Controllers;

use Sober\Controller\Controller;

class Home extends Controller
{
    /**
     * Return images from Advanced Custom Fields
     *
     * @return array
     */
    public function images()
    {
        return 'Hello';
    }

    // Return ACF home slider
    public function homeSlider() {
        $slides = get_field('home_slider');
        return (object) $slides;
    }

    // Return ACF Areas
    public function areas() {
        $areas = get_field('home_areas');
        return (object) $areas;
    }
}
