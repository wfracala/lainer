<?php 
namespace App\Controllers;

use Sober\Controller\Controller;

class Product extends Controller
{
    /**
     * Return images from Advanced Custom Fields
     *
     * @return array
     */
    public function headingText()
    {
        $heading = get_field('heading_text');
        return $heading;
    }

    public function imageModule() {
        $field = get_field('image_module');

        return [
            'heading'   => $field['image_module_headaing'],
            'bg'        => $field['image_module_background']
        ];
    }

    public function hotline() {
        $field = get_field('hotline');

        return [
            'desc'  => $field['hotline_description'],
            'img'   => $field['hotline_image']
        ];
    }

    public function productTraits() {
        $field = get_field('traits');

        return [
            'desc'  => $field['traits_desc'],
            'list'   => $field['traits_list']
        ];
    }

    public function documents() {
        $field = get_field('documents');

        return $field;
    }
}
