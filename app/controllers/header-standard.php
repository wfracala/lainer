<?php 
namespace App\Controllers;

use Sober\Controller\Controller;

class Header extends Controller
{
    // Return ACF home slider
    public function mainImage() {
        $image = get_field('header_image');
        return $image;
    }
}
