export default {
  init() {
    // JavaScript to be fired on all pages

    $('.header__index__slider').slick({
      arrows: true,
    });

    $(".single-project").hover(function() {
      $(this).find(".single-project__content").slideDown();
    }, function () {
      $(this).find(".single-project__content").slideUp();
    });


  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
