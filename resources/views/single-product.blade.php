@extends('layouts.app')

@section('content')
<?php
if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('
	<nav id="breadcrumbs">','</nav>');
}
?>
<div class="row">
  <div class="col-md-3">
    <div class="products-menu">
      @php dynamic_sidebar('sidebar-products') @endphp
      </div>
    @include('partials.'.get_post_type().'.product-documents')
  </div>

  <div class="col-md-9">

    @while(have_posts()) @php the_post() @endphp

      {!! Product::headingText() !!}
      @include('partials.'.get_post_type().'.product-image-module')
      @include('partials.content-single-'.get_post_type())
      @include('partials.'.get_post_type().'.product-traits')
      
    @endwhile

  </div>

  @include('partials.'.get_post_type().'.product-hotline')
</div>

@endsection
