<section class="product-traits">
  <header>
    <ul>
      <li>{{ __('Najważniejsze cechy systemu', 'lainer') }}</li>
      <li>{{ __('Kolorystyka', 'lainer') }}</li>
    </ul>
  </header>
  <div class="product-traits__content">
    <p>
      {{ Product::productTraits()['desc'] }}
    </p>
    <ul>
      @foreach(Product::productTraits()['list'] as $el)
        <li>{{ $el['traits_list_element'] }}</li>
      @endforeach
    </ul>
  </div>
</section>