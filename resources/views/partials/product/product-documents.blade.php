<div class="products-documents">
  <h3>{{ __('Dokumenty do pobrania', 'lainer') }}</h3>

  <ul class="products-documents__list">
    @foreach(Product::documents() as $doc)
      <li><a href="{{ $doc['documents_file']['url'] }}">{{ $doc['documents_file']['title']}}</a></li>
    @endforeach
  </ul>

</div>