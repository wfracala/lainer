<section class="image-module" style="background-image: url({{ Product::imageModule()['bg'] }})">
  <h3>{{ Product::imageModule()['heading'] }}</h3>
</section>