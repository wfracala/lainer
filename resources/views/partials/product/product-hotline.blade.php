  <div class="col-md-12">
      <div class="contact-hotline">
        <div class="wrap">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-9">
                <div class="row">
                  <div class="col-md-1"><img src="{{ Product::hotline()['img'] }}" alt=""></div>
                  <div class="col-md-11">{!! Product::hotline()['desc'] !!}</div>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
  </div>