<header class="banner">
  <div class="header__container container_fluid header__standard">
    <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/logo.png')"></a>
    <div class="header__standard__info">
      @php echo __('Infolina: +48 533 634 000', 'lainer'); @endphp
      <a href="#" class="header__standard__info__search"><img src="@asset('images/search.png')"></a>
      <a href="" class="header__standard__info__lang">Polska</a>
    </div>
    <nav class="nav-primary header__standard__nav">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>

    <div class="header__standard__image" style="background-image: url({{Header::mainImage()}})">
      <div class="wrap">
          <h1>
            {{the_TITLE()}}
          </h1>
      </div>
    </div>
  </div>
</header>