<header class="banner">
  <div class="header__container container_fluid header__index">
    <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/logo.png')"></a>
    <div class="header__index__info">
      @php echo __('Infolina: +48 533 634 000', 'lainer'); @endphp
      <a href="#" class="header__index__info__search"><img src="@asset('images/search.png')"></a>
      <a href="" class="header__index__info__lang">Polska</a>
    </div>
    <nav class="nav-primary header__index__nav">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>

    <div class="header__index__slider">
      @foreach(Home::homeSlider() as $slide)
        <div class="header__index__slider__element">
          <h2>
            @php echo $slide['slide_heading'] @endphp
          </h2>
          <p>
            {{$slide['slide_description']}}
          </p>
        </div>
      @endforeach
    </div>

    <div class="header__index__areas">
      <div class="header__index__areas__boxes">
        @foreach (Home::areas() as $area)
          <a href="{{$area['link_to']}}" class="header__index__areas__boxes__element" style="background: {{$area['box_color']}};border: 5px {{$area['box_border']}} solid">
            <p>{{$area['area_']}}</p>
          </a>
        @endforeach
        </div>
    </div>
  </div>
</header>
