<footer class="content-info">
  <div class="wrap">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12"><hr /></div>
      </div>
      
      <div class="row">
        <div class="col-md-2">@php dynamic_sidebar('sidebar-footer1') @endphp</div>
        <div class="col-md-2">@php dynamic_sidebar('sidebar-footer2') @endphp</div>
        <div class="col-md-2">@php dynamic_sidebar('sidebar-footer3') @endphp</div>
        <div class="col-md-2">@php dynamic_sidebar('sidebar-footer4') @endphp</div>
        <div class="col-md-2">@php dynamic_sidebar('sidebar-footer5') @endphp</div>
        <div class="col-md-2">@php dynamic_sidebar('sidebar-footer6') @endphp</div>
      </div>

      <div class="row banners">
        <div class="col"><img src="@asset('images/social.png')"></div>
        <div class="col"><img src="@asset('images/kapital.png')"></div>
        <div class="col"><img src="@asset('images/w_klaster.png')"></div>
        <div class="col"><img src="@asset('images/klaster.png')"></div>
        <div class="col"><img src="@asset('images/logo_white.png')"></div>
      </div>
      
      <div class="row copyright">
        <div class="col-md-2 lm"><a href="http://lifemotion.pl">lifemotion.pl</a></div>
        <div class="col-md-8 cp">© 2018 LAINER. Wszystkie prawa zastrzeżone. | Polityka prywatności</div>
      </div>
      
    </div>
  </div>
</footer>
