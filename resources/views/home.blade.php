{{--
  Template Name: Homepage
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <div class="front-page">
      <section>
        <h2>Nasza działalność</h2>
        <div class="row">
          <div class="col-md-8">
            <div class="front-page__welcome">
              <h3>Witaj w serwisie Lainer<h3>
                <div class="front-page__welcome__section">
                  <span>
                    Jesteśmy polskim producentem nowoczesnych 
                    żywicznych systemów posadzkowych.
                  </span>
                  
                  <p>
                    Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. 
                    Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies 
                    porta urna.
                  </p>
                  
                  <a class="btn front-page__welcome__button" href="#" role="button">
                    dowiedz się więcej
                  </a>
                  
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="front-page__right-image" style="background-image: url(wp-content/uploads/2018/08/shutterstock_257558758.png)"></div>
            </div>
          </div>
        </section>
        
        <section>
          <div class="row">
            <div class="col-md-8">
              <div class="front-page__section2">
                <div class="row">
                  <div class="col-md-4 front-page__section2__element">
                    <h3>100%</h3>
                    <p>
                      Jesteśmy producentem ze 100% udziałem polskiego kapitału i tu inwestujemy
                    </p>
                  </div>
                  <div class="col-md-4 front-page__section2__element">
                    <h3>20 lat</h3>
                    <p>
                      Ponad 20 letnie 
                      doświadczenie 
                      związane z posadzkami 
                      przemysłowymi                      
                    </p>                
                  </div>
                  <div class="col-md-4 front-page__section2__element">
                    <h3>2018</h3>
                    <p>
                      W 2018 roku nasza firma 
                      uruchamia nowy zakład 
                      produkcyjny        
                    </p>        
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="front-page__section3">
                <p>
                  „Jesteśmy polskim
                  producentem
                  nowoczesnych, żywicznych 
                  systemów posadzkowych”
                </p>
              </div>
            </div>
          </div>
        </section>

      <section>
        @include('partials.content-page')
      </section>
    
    </div>

  @endwhile
@endsection
