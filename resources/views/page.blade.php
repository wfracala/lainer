@extends('layouts.app')

@section('content')
<?php
if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('
	<nav id="breadcrumbs">','</nav>');
}
?>
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')
  @endwhile
@endsection
