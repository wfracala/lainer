<?php 
/*
Element Description: SL Team Member
*/

class vcProjectsWidgets extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'projects_list_mapping' ) );
        add_shortcode( 'projects_list', array( $this, 'projects_list_html' ) );
    }
     
    // Element Mapping
    public function projects_list_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
                return;
        }
             
        // Map the block with vc_map()
        vc_map( 
      
            array(
                'name' => __('Projects widget', 'text-domain'),
                'base' => 'projects_list',
                'description' => __('Element with background and projects in carousel', 'text-domain'), 
                'category' => __('Lainer', 'text-domain'),   
                //'icon' => get_stylesheet_directory_uri().'/assets/images/kacpix.png',            
                //'as_child' => array('only' => 'sl_team'),
                'params' => array( 
                    array(
                        'type' => 'attach_image',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Image', 'text-domain' ),
                        'param_name' => 'image',
                        'description' => __( 'Image', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    )/*,                          
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content2',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Content', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    )*/
                )
            )
        );            
    }
     
     
    // Element HTML
    public function projects_list_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'content2' => '',
                ), 
                $atts
            )
        );
        
        $profile_img = wp_get_attachment_image_src($image,'full')[0];

        // Fill $html var with data
        $html = '
                <div class="row">
          <div class="col-md-12">
            <div class="front-page__section4" style="background-image: url('.$profile_img.')">
              <h3>
                <span>Zrealizowane</span>
                <span>projekty lainer</span>
              </h3>
              <a href="#" class="btn btn-purple">zobacz wszystkie</a>

              <div class="front-page__section4__projects">
                <h3>
                  Lorem ipsum dolor sit amet enim.
                </h3> 
                <p>
                  Etiam ullamcorper. Suspendisse 
                  a pellentesque dui, non felis. 
                  Maecenas malesuada elit lectus felis, malesuada ultricies. 
                </p>
                <p>
                  Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor.
                </p>
                <p class="front-page__section4__projects__arrows">
                  <img src="'.get_template_directory_uri().'/assets/images/nav_l_arrow.png">
                  <img src="'.get_template_directory_uri().'/assets/images/nav_r_arrow.png">
                </p>
              </div>
            </div>
          </div>
        </div>
        
        ';     
        
        return $content.$html;
         
    } 
     
} // End Element Class
 
// Element Class Init
new vcProjectsWidgets();    