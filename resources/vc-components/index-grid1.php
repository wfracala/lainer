<?php 
/*
Element Description: SL Team Member
*/

class vcIndexGrid1 extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'index_grid1_mapping' ) );
        add_shortcode( 'index_grid1', array( $this, 'index_grid1_html' ) );
    }
     
    // Element Mapping
    public function index_grid1_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
                return;
        }
             
        // Map the block with vc_map()
        vc_map( 
      
            array(
                'name' => __('Index grid el.1', 'text-domain'),
                'base' => 'index_grid1',
                'description' => __('Element for index grid', 'text-domain'), 
                'category' => __('Lainer', 'text-domain'),   
                //'icon' => get_stylesheet_directory_uri().'/assets/images/kacpix.png',            
                //'as_child' => array('only' => 'sl_team'),
                'params' => array(   
                    array(
                        'type' => 'attach_image',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Image', 'text-domain' ),
                        'param_name' => 'image',
                        'description' => __( 'Image', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),                          
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Number', 'text-domain' ),
                        'param_name' => 'number',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Number to show', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),                                                
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Heading', 'text-domain' ),
                        'param_name' => 'heading',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Heading text', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),                        
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Description', 'text-domain' ),
                        'param_name' => 'desc',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Description of block', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),
                    array(
                        'type' => 'dropdown',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Direction', 'text-domain' ),
                        'param_name' => 'dir',
                        'value'       => array(
                          'Image on left'   => 'ltr',
                          'Image on right'   => 'rtl'
                        ),
                        'description' => __( 'Direction (image on left or right)', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'std' => 'ltr',
                        'group' => 'Custom Group',
                    )                          
                )
            )
        );            
    }
     
     
    // Element HTML
    public function index_grid1_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'number' => '',
                    'heading' => '',
                    'desc' => '',
                    'dir' => '',
                ), 
                $atts
            )
        );
        
        $profile_img = wp_get_attachment_image_src($image, 'full')[0];
        $dir = trim($dir);

        // Fill $html var with data
        $html = '';     
        
        if($dir == 'rtl') {
          $html = '
          <div class="row index-grid__element1 index-grid__element1__rtl">
            <div class="col-md-6 el2 rtl">
              <div class="el2__content">
                <h3>'.$number.'</h3>
                <header>'.$heading.'</header>
                <p>'.$desc.'</p>
              </div>
            </div>
           <div class="col-md-6 el1">
              <div class="el1__image" style="background-image: url('.$profile_img.')"></div>
            </div>            
          </div>
          '; 
        }
        else {
          $html = '
          <div class="row index-grid__element1">
            <div class="col-md-6 el1">
              <div class="el1__image" style="background-image: url('.$profile_img.')"></div>
            </div>
            <div class="col-md-6 el2 ltr">
              <div class="el2__content">
                <h3>'.$number.'</h3>
                <header>'.$heading.'</header>
                <p>'.$desc.'</p>
              </div>
            </div>
          </div>
          ';           
        }

        return $html;
         
    } 
     
} // End Element Class
 
// Element Class Init
new vcIndexGrid1();    