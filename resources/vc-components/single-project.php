<?php 
/*
Element Description: SL Team Member
*/

class vcSingleProject extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'single_project_mapping' ) );
        add_shortcode( 'single_project', array( $this, 'single_project_html' ) );
    }
     
    // Element Mapping
    public function single_project_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
                return;
        }
             
        // Map the block with vc_map()
        vc_map( 
      
            array(
                'name' => __('Single project', 'text-domain'),
                'base' => 'single_project',
                'description' => __('Single project included on "obszary zastosowan" page', 'text-domain'), 
                'category' => __('Lainer', 'text-domain'),   
                //'icon' => get_stylesheet_directory_uri().'/assets/images/kacpix.png',            
                //'as_child' => array('only' => 'sl_team'),
                'params' => array( 
                    array(
                        'type' => 'textfield',
                        'holder' => 'h3',
                        'class' => 'title-class',
                        'heading' => __( 'Heading', 'text-domain' ),
                        'param_name' => 'heading',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Heading text', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),                   
                    array(
                        'type' => 'attach_image',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Image', 'text-domain' ),
                        'param_name' => 'image',
                        'description' => __( 'Image', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),                        
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Description', 'text-domain' ),
                        'param_name' => 'desc',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Description of block', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Link to', 'text-domain' ),
                        'param_name' => 'url',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'URL', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),
                )
            )
        );            
    }
     
     
    // Element HTML
    public function single_project_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'heading' => '',
                    'desc' => '',
                    'url' => '',
                ), 
                $atts
            )
        );
        
        $profile_img = wp_get_attachment_image_src($image,'full')[0];

        // Fill $html var with data
        $html = '
                <div class="row">
          <div class="col-md-12">
            <div class="single-project" style="background-image: url('.$profile_img.')">
              <header class="single-project__header">
                <h4>
                  '.$heading.'
                </h4>
              </header>
              <div class="single-project__line"></div>

              <div class="single-project__content">
                '.$desc.'

                <br/><a href="'.$url.'" class="btn btn-purple">dowiedz się więcej</a>
              </div>
            </div>
          </div>
        </div>
        
        ';     
        
        return $content.$html;
         
    } 
     
} // End Element Class
 
// Element Class Init
new vcSingleProject();    