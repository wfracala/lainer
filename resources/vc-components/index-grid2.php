<?php 
/*
Element Description: SL Team Member
*/

class vcIndexGrid2 extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'index_grid2_mapping' ) );
        add_shortcode( 'index_grid2', array( $this, 'index_grid2_html' ) );
    }
     
    // Element Mapping
    public function index_grid2_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
                return;
        }
             
        // Map the block with vc_map()
        vc_map( 
      
            array(
                'name' => __('Index grid el.2', 'text-domain'),
                'base' => 'index_grid2',
                'description' => __('Element for index grid', 'text-domain'), 
                'category' => __('Lainer', 'text-domain'),   
                //'icon' => get_stylesheet_directory_uri().'/assets/images/kacpix.png',            
                //'as_child' => array('only' => 'sl_team'),
                'params' => array(   
                    array(
                        'type' => 'attach_image',
                        'holder' => 'div',
                        'class' => 'text-class',
                        'heading' => __( 'Image', 'text-domain' ),
                        'param_name' => 'image',
                        'description' => __( 'Image', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    ),                          
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'title-class',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content2',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Content', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Custom Group',
                    )
                )
            )
        );            
    }
     
     
    // Element HTML
    public function index_grid2_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'content2' => '',
                ), 
                $atts
            )
        );
        
        $profile_img = wp_get_attachment_image_src($image,'full')[0];

        // Fill $html var with data
        $html = '<div class="index-grid__element2" style="background-image: url('.$profile_img.')"><p class="index-grid__element2__content">'.$content2.'</p></div>';     
        
        return $content.$html;
         
    } 
     
} // End Element Class
 
// Element Class Init
new vcIndexGrid2();    