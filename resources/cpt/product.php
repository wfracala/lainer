<?php

if (! function_exists('products')) {
  // Register Custom Post Type
  function products() {

    $labels = array(
      'name'                  => _x('Produkty', 'Post Type General Name', 'lainer'),
      'singular_name'         => _x('Produkt', 'Post Type Singular Name', 'lainer'),
      'menu_name'             => __('Produkty', 'lainer'),
      'name_admin_bar'        => __('Produkt', 'lainer'),
      'archives'              => __('Product Archives', 'lainer'),
      'attributes'            => __('Product Attributes', 'lainer'),
      'parent_item_colon'     => __('Rodzic:', 'lainer'),
      'all_items'             => __('Wszystkie produkty', 'lainer'),
      'add_new_item'          => __('Dodaj produkt', 'lainer'),
      'add_new'               => __('Dodaj nowy', 'lainer'),
      'new_item'              => __('Nowy produkt', 'lainer'),
      'edit_item'             => __('Edit Product', 'lainer'),
      'update_item'           => __('Update Product', 'lainer'),
      'view_item'             => __('View Product', 'lainer'),
      'view_items'            => __('View Product', 'lainer'),
      'search_items'          => __('Search Product', 'lainer'),
      'not_found'             => __('Not found', 'lainer'),
      'not_found_in_trash'    => __('Not found in Trash', 'lainer'),
      'featured_image'        => __('Featured Image', 'lainer'),
      'set_featured_image'    => __('Set featured image', 'lainer'),
      'remove_featured_image' => __('Remove featured image', 'lainer'),
      'use_featured_image'    => __('Use as featured image', 'lainer'),
      'insert_into_item'      => __('Insert into Product', 'lainer'),
      'uploaded_to_this_item' => __('Uploaded to this Product', 'lainer'),
      'items_list'            => __('Products list', 'lainer'),
      'items_list_navigation' => __('Products list navigation', 'lainer'),
      'filter_items_list'     => __('Filter Products list', 'lainer'),
    );
    $args = array(
      'label'                 => __('Product', 'lainer'),
      'description'           => __('Products', 'lainer'),
      'labels'                => $labels,
      'supports'              => array('title', 'editor', 'thumbnail'),
      'taxonomies'            => array('product_cat'),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => 'dashicons-cart',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );
    register_post_type('product', $args);
  }
  add_action('init', 'products', 0);
}

if (! function_exists('product_cat')) {
  // Register Custom Taxonomy
  function product_cat() {

    $labels = array(
      'name'                       => _x('Kategorie produktów', 'Taxonomy General Name', 'lainer'),
      'singular_name'              => _x('Product Category', 'Taxonomy Singular Name', 'lainer'),
      'menu_name'                  => __('Product Categories', 'lainer'),
      'all_items'                  => __('All Product Categories', 'lainer'),
      'parent_item'                => __('Parent Product Category', 'lainer'),
      'parent_item_colon'          => __('Parent Product Category:', 'lainer'),
      'new_item_name'              => __('New Product Category', 'lainer'),
      'add_new_item'               => __('Add Product Category', 'lainer'),
      'edit_item'                  => __('Edit Product Category', 'lainer'),
      'update_item'                => __('Update Product Category', 'lainer'),
      'view_item'                  => __('View Product Category', 'lainer'),
      'separate_items_with_commas' => __('Separate items with commas', 'lainer'),
      'add_or_remove_items'        => __('Add or remove items', 'lainer'),
      'choose_from_most_used'      => __('Choose from the most used', 'lainer'),
      'popular_items'              => __('Popular Items', 'lainer'),
      'search_items'               => __('Search Items', 'lainer'),
      'not_found'                  => __('Not Found', 'lainer'),
      'no_terms'                   => __('No items', 'lainer'),
      'items_list'                 => __('Items list', 'lainer'),
      'items_list_navigation'      => __('Items list navigation', 'lainer'),
     );
    $rewrite = array(
      'slug'                       => 'products',
      'with_front'                 => true,
      'hierarchical'               => true,
    );
    $args = array(
      'labels'                     => $labels,
      'hierarchical'               => true,
      'public'                     => true,
      'show_ui'                    => true,
      'show_admin_column'          => true,
      'show_in_nav_menus'          => false,
      'show_tagcloud'              => false,
      'rewrite'                    => $rewrite,
    );
    register_taxonomy('product_cat', array('product'), $args);
  }
  add_action('init', 'product_cat', 0);
}
